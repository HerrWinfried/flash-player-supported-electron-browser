const electron = require("electron");
const url = require("url");
const path = require("path");

const { app, BrowserWindow } = electron;

let mainWindow;
let pluginName
let pluginVersion
switch (process.platform) {
  case 'win32':
    pluginName = 'pepflashplayer.dll'
    pluginVersion = '32.0.0.453'
    break
  case 'darwin':
    pluginName = 'PepperFlashPlayer.plugin'
pluginVersion = '32.0.0.207'
    break
  case 'linux':
    pluginName = 'libpepflashplayer.so'
pluginVersion = '32.0.0.465'
    break
}

app.commandLine.appendSwitch('ppapi-flash-path', path.join(__dirname, pluginName))
app.commandLine.appendSwitch('ppapi-flash-version', `${pluginVersion}`);

app.whenReady().then(() => {
    let win = new BrowserWindow({
      width: 800,
      height: 600,
      webPreferences: {
        plugins: true
      }
    })
    win.loadURL(`https://vice-hotel.com/`)
    // Something else
  })

/*app.on("ready", () => {
mainWindow = new BrowserWindow({
    'width': 800,
    'height': 600,
    'web-preferences': {
      'plugins': true,
      'nodeIntegration': true
    }
  });
mainWindow.loadURL("https://isflashinstalled.com/");

})*/
